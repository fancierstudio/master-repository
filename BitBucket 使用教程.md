# BitBucket 使用教程

## 1 什么是版本控制和代码托管？



## 2 为什么选择 BitBucket？

* BitBucket 提供无限的文本文件存储空间和 10 GB 的大型文件存储空间，对于 5 人以下的团队完全免费，并且有完善的权限管理功能。
* 与 GitHub 之类带有开源社交基因的代码托管网站相比，BitBucket 更加私密，对于授权协议也更加宽松。
* BitBucket 的母公司 Atlassian 推出的 SourceTree 是目前跨平台最好用的 Git 客户端。
* 与 GitHub 相比，BitBucket 提供中文界面，在国内的连接性尚可，虽然曾经出现过被墙的情况，现在访问时常也会很慢，但相对还是比较稳定的，甚至超过部分国内托管网站。退一万步说，科学上网也是一项必备技能，对于开发者 (哪怕是 mod 的开发者) 来说尤为重要。



## 3 网站注册和基本介绍

如果只查看项目页面、下载文件而不修改的话不需要注册账号，以 CU 为例，进入 https://bitbucket.org/fancierstudio/master-repository/downloads/，可以下载某个标签、分支或整个库。

### 3.1 注册

在 BitBucket 首页 (https://bitbucket.org) 点击 ｢Get started for free｣。

![1](https://pic.pimg.tw/rx1226/1460729358-1756615406_n.png)

或直接输入链接 https://bitbucket.org/account/signup/ 进入注册页面。

输入你的 Email 地址 (最好使用 Gmail, Outlook 等国外邮箱)，点击 ｢Continue｣。

![2](https://pic.pimg.tw/rx1226/1460729359-1597085212.png)

到邮箱接收确认信，如果没有收到邮件请检查垃圾信箱。

![3](https://pic.pimg.tw/rx1226/1460729359-1322937140.png)

然后输入用户名和密码。

![4](https://pic.pimg.tw/rx1226/1460729360-2959611571.png)

登录后看到起始页面就成功了。

![5](https://pic.pimg.tw/rx1226/1460729360-3678320000_n.png)

### 3.1.2 切换中文界面

虽然 BitBucket 的默认界面是英文，但是在登录后可以转成中文。

点击右上角的头像，

![6](http://ww4.sinaimg.cn/large/006tKfTcgy1feq2kr8kmjj305x07faac.jpg)

![7](http://ww3.sinaimg.cn/large/006tKfTcgy1feq2n2od47j30fp0d3my1.jpg)

## 4 使用 SourceTree

## 5 进一步了解 Git

