mill = {
	cost = 80
	time = 12
	trigger = {
                OR = {
		            has_terrain = farmlands
					has_terrain = grasslands
					has_terrain = woods
					has_terrain = hills
					has_province_modifier = ti_marsh_done 
					has_province_modifier = holland_polders
					has_province_modifier = ti_irragation_done
					has_province_modifier = ti_terrace_done
					has_province_modifier = ti_jungle_done
					has_province_modifier = ti_forest_done
                }
	}
	modifier = {
		local_development_cost = -0.05
        trade_goods_size = 0.05
		allowed_num_of_buildings = 1
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 2
			NOT = { base_manpower = 4 }
			development = 10
		}
		modifier = {
			factor = 0.5
			NOT = { check_variable = { which = r_ac value = 0 } }
		}	
		modifier = {
			factor = 1.5
			development = 8
		}
		modifier = {
			factor = 1.5
			development = 12
		}
		modifier = {
			factor = 1.5
			development = 16
		}
		modifier = {
			factor = 2
			OR = {
			       trade_goods = grain
				   trade_goods = wine
				   trade_goods = fish
			}
		}
		modifier = {
			factor = 0.5
			NOT = { has_owner_culture = yes }
			NOT = { has_owner_accepted_culture = yes }
			NOT = { owner = { culture_group = ROOT } }
		}
	}
}