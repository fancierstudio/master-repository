# Do not change tags in here without changing every other reference to them.
# Do not change tags in here without changing every other reference to them.
# If adding new groups or ideas, make sure they are unique.

# ai_will do is only used to determine which idea group is picked if AI_USES_HISTORICAL_IDEA_GROUPS define is 0, OR start is custom/random
# ai never picks an idea that resolves to ai_will_do = 0, even if AI_USES_HISTORICAL_IDEA_GROUPS = 1
# ROOT = country picking the idea
# groups set to colonial=yes will be higher prioritized by the AI when spending power

aristocracy_ideas = {
	category = MIL

	bonus = {
		free_leader_pool = 30
	}

	trigger = {
		OR = {
			government = monarchy
			government = noble_republic
			government = theocracy
		}
		
		
	}	
	noble_knights = {
		cavalry_power = 0.1
		cavalry_flanking = 0.20
	}
	local_nobility = {
        vassal_income = 0.15
		cav_to_inf_ratio = 0.10
	}
	serfdom = {
		global_manpower_modifier = 0.20
	}
	noble_officers = {
		leader_land_shock = 1
	}
    international_nobility = {
		diplomats = 1
	}
	noble_resilience = {
        war_exhaustion_cost = -0.20
	}	
    military_traditions = {
		land_morale = 0.10
	}
	ai_will_do = {
		factor = 0.25
		
		modifier = {
			factor = 0
			is_subject = yes
		}	
	}
}


plutocracy_ideas = {
	category = MIL

	bonus = {
                republican_tradition = 0.4
                free_leader_pool = 15
	}

	trigger = {
		NOT = { government = monarchy }
		NOT = { government = noble_republic }
		NOT = { government = theocracy }
	}
	
	tradition_of_payment = {
                mercenary_cost = -0.25
	}
	abolished_serfdom = {
		land_morale = 0.10
	}
	bill_of_rights = {
		global_unrest = -1
		global_institution_spread = 0.10
	}
	free_merchants = {
		merchants = 1
	}
	free_cities = {
		caravan_power = 0.3
	}
	emancipation = {
		manpower_recovery_speed = 0.20
	}
	meritocracy_cu = {
		advisor_pool = 1
                #-20% GC Loss From Autonomy
	}
	ai_will_do = {
		factor = 0.4
	}
}


innovativeness_ideas = {
	category = ADM
    trigger = {
		NOT = { religion_group = eastern }
		NOT = { religion_group = dharmic }
        NOT = { religion_group = pagan }
		NOT = { has_idea_group = reform_ideas }
	}
	bonus = {
	    global_institution_spread = 0.2
	}
	
	pragmatism_2 = {
		idea_cost = -0.05
	}
	patron_of_art  = { 
        prestige = 1
	}
	resilient_state = {
		inflation_action_cost = -0.2
	}	
	optimism = {
		war_exhaustion = -0.05
	}
	formalized_officer_corps_2 = {
		leader_siege = 1
	}
	dynamic_court_2 = {
	    advisor_cost = -0.05 #Now Primary Culture OGC Cost
        #+50% Capital BGC
	}
	scientific_revolution_2 = {
		technology_cost = -0.05
		#Literacy Growth: +25%
	}
	ai_will_do = {
		factor = 0.3
		modifier = {
			factor = 2
			government = steppe_horde
		}
		modifier = {
			factor = 2
			num_of_cities = 20
		}
		modifier = {
			factor = 2
			num_of_cities = 50
		}
	}
}


religious_ideas = {
	category = ADM

	bonus = {
		culture_conversion_cost = -0.15
	}

        trigger = {
		NOT = { religion_group = eastern }
		NOT = { religion_group = dharmic }
                NOT = { religion_group = pagan }
		NOT = { has_idea_group = tradition_ideas }
		NOT = { has_idea_group = humanist_ideas }
	}
        devoutness = {
		tolerance_own = 1
	}
        religious_tradition = {
                prestige_decay = -0.01
		papal_influence = 1
		devotion = 0.5
		monthly_fervor_increase = 0.25
		church_power_modifier = 0.1
	}
        
        church_attendance_duty = {
		stability_cost_modifier = -0.20
	}
        inquisition = {
		global_missionary_strength = 0.01
	}
	missionary_schools = {
		missionaries = 1
	}
	divine_supremacy = { 
                global_heretic_missionary_strength = 0.01
	}
	deus_vult = {
		cb_on_religious_enemies = yes
	}
	ai_will_do = {
		factor = 0.9
		modifier = {
			factor = 0
			has_idea_group = humanist_ideas
		}			
	}
}


tradition_ideas = {
	category = ADM

	bonus = {
            culture_conversion_cost = -0.15
	}

	trigger = {
		OR = { 
                        religion_group = eastern 
		        religion_group = dharmic 
                        religion_group = pagan
                }
		NOT = { has_idea_group = religious_ideas }
		NOT = { has_idea_group = reform_ideas }
	}
	
	oral_tradition = {
                tolerance_own = 1
	}
	one_with_nature = {
		development_cost = -0.05
	}
	polytheism = {
		no_religion_penalty = yes
		yearly_harmony = 0.2
	}
	religious_conversation = {
		global_missionary_strength = 0.01
		num_accepted_cultures = 1
	}
	way_of_warrior = {
		war_exhaustion = -0.04
	}
	inward_perfection_idea = {
		stability_cost_modifier = -0.20
	}
	circle_of_destiny = {
		legitimacy = 1
                republican_tradition = 0.3
                horde_unity = 1
                devotion = 1
                #Powerful CB against aggressive neighbor
	}
	
	ai_will_do = {
		factor = 1
	}
}

reform_ideas = {
	category = ADM

	bonus = {
                years_of_nationalism = -5
	}

	trigger = {
		OR = { 
                        religion_group = eastern 
		        religion_group = dharmic 
                        religion_group = pagan
                }
                NOT = { has_idea_group = innovativeness_ideas }
				NOT = { has_idea_group = tradition_ideas }
	}
	
	the_first_outsight = {
        global_institution_spread = 0.10
	}
    religious_traveller = {
		missionaries = 1
	}
    gunpowder_warfare = {
		infantry_power = 0.1
	}
    rational_thinking = {
		#Literacy Growth: +0.01
        idea_cost = -0.05       
	}
	sea_power_consciousness = {
                global_ship_cost = -0.10
        }
    custom_shift = {
        embracement_cost = -0.2        
	}
        the_chosen_path = {
		province_warscore_cost = -0.1
                #CB for all coastal provinces.
	}
	ai_will_do = {
		factor = 1
	}
}



spy_ideas = {
	category = DIP

	bonus = {
		rebel_support_efficiency = 0.50
		harsh_treatment_cost = -0.25
	}

	efficient_spies = {
		spy_offence = 0.33
	}	
		
	agent_training = {
		diplomats = 1		
	}

	vetting = {
		global_spy_defence = 0.33
	}	
	

	claim_fabrication = {
		fabricate_claims_cost = -0.25
	}
	
	infiltration = {
	    siege_ability = 0.10
	}
	audit_checks = {
		yearly_corruption = -0.1
	}
	
	additional_loyalist_recruitment_2 = {
	    global_unrest = -1
		#-25% OGC from foreign cultures and religions
	}

	ai_will_do = {
		factor = 0.75
	}
}

diplomatic_ideas = {
	category = DIP
	bonus = {
		reduced_stab_impacts = yes
		can_chain_claim = yes
	}

	foreign_embassies = {
		diplomats = 1
	}
	cabinet = {
		diplomatic_upkeep = 1
	}
	benign_diplomats = {
		improve_relation_modifier = 0.3
	}
	experienced_diplomats  = {
		diplomatic_reputation = 2
	}
    war_cabinet = {
        war_exhaustion_cost = -0.2
	}		
	flexible_negotiation = {
		province_warscore_cost = -0.15
		unjustified_demands = -0.25
	}
	diplomatic_corps = {
		dip_tech_cost_modifier = -0.1
	}
	
	ai_will_do = {
		factor = 0.67

		modifier = {
			factor = 2
			is_emperor = yes
		}	
		modifier = {
			factor = 1.5
			vassal = 2
		}		
	}
}


offensive_ideas = {
	category = MIL

	bonus = {
        land_attrition = -0.20
        free_leader_pool = 15
	}

	bayonet_leaders = {
		leader_land_shock = 1
	}
    glorious_arms = {
		prestige_from_land = 0.50
		army_tradition_from_battle = 0.33
	}
	superior_firepower = {
		leader_land_fire = 1
	}
	war_economy = {
        loot_amount = 0.50
	}
	engineer_corps = {
		siege_ability = 0.2
	}
	grand_army = {
		land_forcelimit_modifier = 0.25
	}
	napoleonic_warfare = {
		discipline = 0.05
	}
	
	ai_will_do = {
		factor = 0.94
	}
}


defensive_ideas = {
	category = MIL

	bonus = {
                fort_maintenance_modifier = -0.2
                free_leader_pool = 15
	}

	battlefield_commisions = {
		army_tradition = 1
	}
	military_drill = {
		land_morale = 0.15
	}
	improved_manuever = {
		leader_land_manuever = 1
		movement_speed = 0.05
	}
	regimental_system = {
                global_regiment_recruit_speed = -0.1
                garrison_size = 0.20
	}	
	defensive_mentality = {
		defensiveness = 0.25
	}
	supply_trains = {
		reinforce_speed = 0.30
	}
	patriotism = {
		recover_army_morale_speed = 0.05
                #Allows Mobilization
	}
	ai_will_do = {
		factor = 0.77
	}
}


trade_ideas = {
	category = DIP

	bonus = {
		trade_steering = 0.3
	}	
	
	shrewd_commerce_practise = {
 		global_trade_power = 0.10
  	}
  	free_trade = {
  		merchants = 1
  	}
	merchant_adventures = {
		trade_range_modifier = 0.5
	}
	national_trade_policy = {
		trade_efficiency = 0.10
	}
	overseas_merchants = {
		merchants = 1
	}
	trade_manipulation_2 = {
		caravan_power = 0.25
        #Trading Company Upkeep:-10%
	}
	the_feitoria = {
        global_foreign_trade_power = 0.10
		#Commerce Growth Modifier: +25%
	}
	
	ai_will_do = {
		factor = 0.7
		modifier = {
			factor = 0
			NOT = { num_of_cities = 6 }
			NOT = { government = merchant_republic }
		}
		modifier = {
			factor = 3
			government = merchant_republic
		}
	}
}




economic_ideas = {
	category = ADM

	bonus = {
		development_cost = -0.10
	}	
    centralized_tax_collection = {
		global_tax_modifier = 0.10
	}
	national_bank = {
        inflation_action_cost = -0.2
		#inflation_reduction = 0.1
	}
	debt_and_loans = {
		interest = -1
	}
	nationalistic_enthusiasm = {
		land_maintenance_modifier = -0.10
	}
	smithian_economics = {
		production_efficiency = 0.1
 	}
    benign_neglect_2 = {
        global_trade_goods_size_modifier = 0.05
		#Production Growth: +25%
	}
	organised_construction = {
		build_cost = -0.10
	}
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1.5
			inflation = 5
		}
		modifier = {
			factor = 1.5
			inflation = 10
		}
		modifier = {
			factor = 3
			government = steppe_horde
		}
		modifier = {
			factor = 1.3
			num_of_cities = 20
		}
		modifier = {
			factor = 1.3
			num_of_cities = 50
		}
	}
}


exploration_ideas = {
	category = DIP
	important = yes

	bonus = {
		cb_on_primitives = yes
	}
	
	colonial_ventures = {
		colonists = 1
	}
	quest_for_the_new_world	= {
		may_explore = yes
	}
	overseas_exploration = {
		range = 0.5
	}	
	land_of_opportunity = {
		global_colonial_growth = 15
	}
    free_colonies = {
		colonist_placement_chance = 0.05
	}
	vice_roys_2  = {
 		global_tariffs = 0.15
		#-25%OGC by colonial subjects.
 	}
	global_empire = {
		naval_forcelimit_modifier = 0.25
                naval_attrition = -0.1
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			is_subject = yes
		}		
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { 
				any_owned_province = { 
					is_in_capital_area = yes
					any_neighbor_province = {
						is_empty = yes
					}
				} 
			}
		}
		modifier = {
			factor = 0.1
			NOT = { num_of_ports = 3 }
			NOT = { 
				any_owned_province = { 
					is_in_capital_area = yes
					any_neighbor_province = {
						is_empty = yes
					}
				} 
			}			
		}
		modifier = {
			factor = 0.1
			NOT = { has_institution = renaissance }
			NOT = { tag = POR }
			NOT = { tag = CAS }
		}
		modifier = {
			factor = 0.1
			capital_scope = {
				NOT = {
					region = france_region
					region = iberia_region
					region = british_isles_region
					region = low_countries_region
				}
			}
			NOT = { technology_group = high_american }
		}
		modifier = {
			factor = 0.5
			tag = ARA
		}
		modifier = {
			factor = 0.1
			NOT = { is_year = 1490 }
			capital_scope = {
				NOT = {
					region = iberia_region
				}
			}
		}
		modifier = {
			factor = 10
			technology_group = high_american
		}
		modifier = {
			factor = 2.0
			num_of_ports = 5	
		}			
		modifier = {
			factor = 2.0
			num_of_ports = 10		
		}			
		modifier = {
			factor = 2.0
			num_of_ports = 15		
		}	
		modifier = {
			factor = 2.0
			num_of_ports = 20
		}
		modifier = {
			factor = 1000
			OR = {
				tag = POR
				tag = CAS
				tag = SPA
			}
		}
		modifier = {
			factor = 2
			OR = {
				tag = GBR
				tag = NED
				tag = ENG
				tag = FRA
				tag = KUR
			}
		}
	}
}


maritime_ideas = {
	category = DIP

	bonus = {
		sea_repair = yes
	}

	sea_hawks = {
		navy_tradition = 1
	}
	merchant_marine = {
                global_ship_trade_power = 0.15
                light_ship_power = 0.15
	}
	sheltered_ports = {
		global_ship_repair = 0.25
	}
	grand_navy = {
		naval_forcelimit_modifier = 0.25
                global_sailors_modifier = 0.25
	}
	ships_penny = {
		global_ship_cost = -0.10
	}
	excellent_shipwrights = {
		leader_naval_manuever = 1
	}
	naval_fighting_instruction = {
		blockade_efficiency = 0.5
	}
	
	ai_will_do = {
		factor = 0.41
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { is_colonial_nation = yes }
		}
		modifier = {
			factor = 0.41
			NOT = { num_of_ports = 10 }
		}
		modifier = {
			factor = 0.41
			num_of_cities = 50
		}
	}
}


quality_ideas = {
	category = MIL

	bonus = {
		discipline = 0.05
                free_leader_pool = 15
	}
	
	private_to_marshal = {
		infantry_power = 0.1
	}
	quality_education = {
                army_tradition_decay = -0.01
                navy_tradition_decay = -0.01
	}
	finest_of_horses = {
		cavalry_power = 0.1
	}
	naval_drill = {
		naval_morale = 0.10
	}
	copper_bottoms = {
        ship_durability = 0.05
        naval_attrition = -0.1
	}
	massed_battery = {
		artillery_power = 0.1
	}
	superior_weapons = {
		mil_tech_cost_modifier  = -0.1
	}
	ai_will_do = {
		factor = 0.49
	}
}


quantity_ideas = {
	category = MIL

	bonus = {
	        global_manpower_modifier = 0.50
                free_leader_pool = 15
	}
	mass_army = {
                land_forcelimit_modifier  = 0.33
	}
	the_young_can_serve = {
		manpower_recovery_speed = 0.2
	}
	enforced_service = {
		global_regiment_cost = -0.10
	}
	the_old_and_infirm = {
		land_maintenance_modifier = -0.10
	}
	defense_in_depth = {
		shock_damage_received = -0.1
	}
	garrison_conscription = {
		garrison_size = 0.20
                fort_maintenance_modifier = -0.1
        }
	expanded_supply_trains = {
        reinforce_speed = 0.15
		reinforce_cost_modifier = -0.15
	}	
	
	ai_will_do = {
		factor = 1
	}
}


expansion_ideas = {
	category = ADM
	important = yes
	
	bonus = {
 		cb_on_overseas = yes
                envoy_travel_time = -0.20
	}

	faster_colonists = {
		colonists = 1
	}
	additional_colonists_2 = {
                auto_explore_adjacent_to_colony = yes
		#Colonial Development Time: -10%
	}
	organised_recruiting = {
		global_regiment_recruit_speed = -0.10
			global_ship_recruit_speed = -0.10
	}
	additional_diplomats = {
                diplomatic_upkeep = 1
	}	
	additional_merchants = {
		merchants = 1
	}
	competetive_merchants  = {
		global_trade_power = 0.10
	}
	improved_shipyards_2  = {
                #-33% extra OGC from bad terrains and climates
		idea_claim_colonies = yes
	}
	
	ai_will_do = {
		factor = 0.48
		modifier = {
			factor = 10
			OR = {
				has_idea_group = exploration_ideas
				is_colonial_nation = yes
			}
		}
		modifier = {
			factor = 5
			OR = {
				tag = CAS
				tag = POR
				tag = SPA
			}
		}
		modifier = {
			factor = 10
			tag = RUS
			num_of_cities = 20
			any_owned_province = {
				any_neighbor_province = {
					is_empty = yes
				}
			}
		}
		modifier = {
			factor = 0
			NOT = { has_idea_group = exploration_ideas }
			NOT = { is_colonial_nation = yes }
			NOT = {
				any_owned_province = {
					any_neighbor_province = {
						is_empty = yes
					}
				}
			}
		}
		modifier = {
			factor = 0.1
			NOT = { 
				technology_group = western
				technology_group = eastern
				technology_group = muslim
			}
		}				
	}
}


administrative_ideas = {
	category = ADM
	
	bonus = {
	    yearly_corruption = -0.1
	}
	
    bureaucracy_2 = {
		advisor_cost = -0.1
	}
    bookkeeping = {
		interest = -1
	}
	adaptability = {
		core_creation = -0.10
	}	
    civil_servants_2 = {
		max_states = 3
	}
    centralization = {
		global_autonomy = -0.05         
	}
	local_government = {
                state_maintenance_modifier = -0.25
		#+20% GC From Government Buildings
	}
        separation_of_powers = {
                adm_tech_cost_modifier = -0.1
	}
	
	ai_will_do = {
		factor = 0.96
		modifier = {
			factor = 2
			government = steppe_horde
		}		
	}
}

humanist_ideas = {
	category = ADM

	bonus = {
	        improve_relation_modifier = 0.25
                yearly_harmony = 0.25
	}
	
	trigger = {
		NOT = { has_idea_group = religious_ideas }
	}
	tolerance_idea = {
		religious_unity = 0.25
	}	
	local_traditions = {
		global_unrest = -2
	}
	ecumenism = {
		tolerance_heretic = 3
	}	
	indirect_rule = {
		years_of_nationalism = -5
	}
	cultural_ties = {
		num_accepted_cultures = 2
	}
	humanist_tolerance = {
		tolerance_heathen = 3
	}		
	benevolence_2 = {
		female_advisor_chance = 0.25
		#+5%GC Per Tolerance for provinces of a different cultural group
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			has_idea_group = religious_ideas
		}		
	}
}

influence_ideas = {
	category = DIP
	
	bonus = {
	    #prestige = 1
		reduced_liberty_desire = 10
	}

	tribute_system = {
		vassal_income = 0.25
	}
        
	marcher_lords_2 = {
		vassal_forcelimit_bonus = 0.25
        #+25% Vassal Force Limit
	}
	expand_cadet_branches = {
	    heir_chance = 0.25
		diplomatic_upkeep = 1
	}		
	integrated_elites = {
		diplomatic_annexation_cost = -0.20
	}
	state_propaganda = {
		ae_impact = -0.2
	}
	diplomatic_influence = {
		diplomatic_reputation = 2
	}
	postal_service = {
		envoy_travel_time = -0.20
	}	
	
	ai_will_do = {
		factor = 0.5
		modifier = {
			factor = 1.5
			is_emperor = yes
		}
		modifier = {
			factor = 1.7
			vassal = 1
		}
		modifier = {
			factor = 1.7
			vassal = 2
		}
	}	
}

naval_ideas = {
	category = MIL
	
	bonus = {
		ship_durability = 0.1
                free_leader_pool = 20
	}
	
	boarding_parties = {
		leader_naval_shock = 1
		capture_ship_chance = 0.2
	}
	improved_rams = {
		galley_power = 0.20
	}
	naval_cadets = {
		leader_naval_fire = 1
	}
	naval_glory = {
		navy_tradition_decay = -0.01
        prestige_from_naval = 0.5
	}
	press_gangs = {
		sailors_recovery_speed = 0.2	
        naval_maintenance_modifier = -0.10
	}
	oak_forests_for_ships = {
		heavy_ship_power = 0.20
	}
	superior_seamanship = {
		naval_morale = 0.15
	}	
	
	ai_will_do = {
		factor = 0.31
		modifier = {
			factor = 0
			is_tribal = yes
		}
		modifier = {
			factor = 0
			primitives = yes
		}
		modifier = {
			factor = 0
			NOT = { num_of_ports = 1 }
			NOT = { is_colonial_nation = yes }
		}
		modifier = {
			factor = 0.31
			NOT = { num_of_ports = 10 }
		}
		modifier = {
			factor = 0.31
			num_of_cities = 50
		}
	}
}