--NGame
NDefines.NGame.END_DATE = "1836.1.1"				-- at which threshold you can get events

--NDiplomacy

NDefines.NDiplomacy.OVEREXTENSION_THRESHOLD = 2.0				-- at which threshold you can get events
NDefines.NDiplomacy.WARNING_YEARS = 20								-- Years before warning expire
NDefines.NDiplomacy.AE_THREATEN_WAR = 1.25
NDefines.NDiplomacy.PO_DEMAND_PROVINCES_AE = 0.5      -- _DDEF_PO_DEMAND_PROVINCES_AE = 10, (Per development)
NDefines.NDiplomacy.PO_RETURN_CORES_AE = 0.3					-- (Per core, only applied if returning cores to vassals of winner)
NDefines.NDiplomacy.PO_FORM_PU_AE = 0.2						-- _DDEF_PO_FORM_PU_AE = 10, (Per development)
NDefines.NDiplomacy.PO_CHANGE_RELIGION_PRESTIGE = 1 				-- _DDEF_PO_CHANGE_RELIGION_PRESTIGE = 10
NDefines.NDiplomacy.PEACE_COST_RETURN_CORE = 0.9					-- Return a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_BECOME_VASSAL = 0.9					-- Vassalize a country (scales by province wealth)
NDefines.NDiplomacy.ANNEX_DIP_COST_PER_DEVELOPMENT = 11			-- per development
NDefines.NDiplomacy.DIP_PORT_FEES = 0.02  						-- DIP_PORT_FEES
--NDefines.NDiplomacy.FABRICATE_CLAIM_COST = 20
NDefines.NDiplomacy.INFILTRATE_ADMINISTRATION_COST = 50				
NDefines.NDiplomacy.INTEGRATE_VASSAL_MIN_YEARS = 20				-- Number of years before a vassal can be integrated
NDefines.NDiplomacy.TRUST_PENALTY_FOR_SEPARATE_PEACE = 5			-- Trust penalty for signing a separate peace
NDefines.NDiplomacy.TRIBUTE_BASE_CASH = 0.05						-- Tributary State: Part of yearly income given in tribute
NDefines.NDiplomacy.TRIBUTE_BASE_ADM = 0.01						-- Tributary State: Part Adm tribute, mulitplied by total development
NDefines.NDiplomacy.TRIBUTE_BASE_DIP = 0.01						-- Tributary State: Part Dip tribute, mulitplied by total development
NDefines.NDiplomacy.TRIBUTE_BASE_MIL = 0.01
NDefines.NDiplomacy.TRIBUTE_MAX_MONARCH_POWER = 4.0	
NDefines.NDiplomacy.TRIBUTE_BASE_MANPOWER = 0.1

--NDiplomacyEnd

--NCountry

NDefines.NCountry.MIN_DEV_FOR_GREAT_POWER = 500					-- Countries with less development than this cannot be considered Great Powers
NDefines.NCountry.MAX_ACTIVE_POLICIES = 8     					-- how many active policies at once.
NDefines.NCountry.MINIMUM_POLICY_TIME = 20    					-- how many years minimum for a policy
NDefines.NCountry.POLICY_COST = 0         							-- Monthly cost per policy
NDefines.NCountry.WESTERNISATION_THRESHOLD = 9          -- techs behind to be allowed to westernize.
NDefines.NCountry.WESTERN_POWER_TICK = 7          			-- max power to transfer every month.
NDefines.NCountry.WESTERN_POWER_TICK_MIN = 3      			-- min power to transfer every month.
NDefines.NCountry.WESTERN_NEEDED_MAX_POWER = 4000				-- needed for full westernisation
NDefines.NCountry.CULTURE_LOSS_THRESHOLD = 0.00 				-- _CDEF_CULTURE_LOSS_THRESHOLD = 10
NDefines.NCountry.CULTURE_GAIN_THRESHOLD = 0.25 				-- _CDEF_CULTURE_GAIN_THRESHOLD = 10
NDefines.NCountry.LEGITIMACY_DYNASTY_CHANGE = 30				-- Legitimacy a new dynasty starts out with
NDefines.NCountry.POWER_MAX = 1999								      -- how much power can be stored at maximum.
NDefines.NCountry.FREE_IDEA_GROUP_COST  = 4				      -- modifier on cheapness of "free" idea group
NDefines.NCountry.MAX_TOLERANCE_HERETIC = 4 						-- maximum tolerance towards heretics
NDefines.NCountry.MAX_TOLERANCE_HEATHEN = 4  						-- maximum tolerance towards heathens
NDefines.NCountry.CONVERSION_COOLDOWN_SECONDARY = 240		-- months before you can convert secondary religion again.
NDefines.NCountry.IDEA_TO_TECH = -0.01            			-- percentage on tech reduction per idea.
NDefines.NCountry.TECH_TIME_COST = 0.2            			-- tech grow with 20% cost over time.
NDefines.NCountry.PS_ADVANCE_TECH = 700
NDefines.NCountry.PS_MAKE_PROVINCE_CORE = 15
NDefines.NCountry.PS_REDUCE_INFLATION = 100
NDefines.NCountry.PS_REPLACE_RIVAL = 50
NDefines.NCountry.PS_ATTACK_NATIVES = 2
NDefines.NCountry.PS_CHANGE_GOVERNMENT = 200
NDefines.NCountry.PS_CHANGE_CULTURE = 13
NDefines.NCountry.PS_REDUCE_WAREXHAUSTION = 50
NDefines.NCountry.PS_FACTION_BOOST = 33
NDefines.NCountry.PS_LOWER_TARIFFS = 50
NDefines.NCountry.PS_IMPROVE_PROVINCE_BASE = 1
NDefines.NCountry.PS_IMPROVE_PROVINCE_MUL = 0
NDefines.NCountry.PS_IMPROVE_PROVINCE_CAPITAL_DISCOUNT = 0.005
NDefines.NCountry.CORE_HAD_CLAIM = 0.2
NDefines.NCountry.CORE_HAD_PERMANENT_CLAIM = 0.2							-- Impacts MODIFIER_CORE_CREATION
NDefines.NCountry.WAREXHAUSTION_REDUCTION = 1
NDefines.NCountry.PROVINCE_DISCOVERY_PRESTIGE = 5
NDefines.NCountry.CORE_LOSE_PRESTIGE = -5.0
NDefines.NCountry.ABANDON_CORE_PRESTIGE = -5.0
NDefines.NCountry.ABANDON_IDEAGROUP_REFUND = 0.20
NDefines.NCountry.MISSIONARY_PROGRESS_ON_CHANCE = 0.5
NDefines.NCountry.MONTHS_TO_CHANGE_CULTURE = 40
NDefines.NCountry.LIBERTY_DESIRE_HISTORICAL_FRIEND = -30
NDefines.NCountry.LIBERTY_DESIRE_HISTORICAL_RIVAL = 30
NDefines.NCountry.PROTECTORATE_TECH_THRESHOLD = 0.40
NDefines.NCountry.RAZE_UNREST_DURATION = 10
NDefines.NCountry.RAZE_PROVINCE_DEVELOPMENT_DECREASE = 0.25
NDefines.NCountry.RAZE_PROVINCE_POWER_PER_DEVELOPMENT = 10.0
NDefines.NCountry.RAZE_TECH_POWER_DECREASE = 0.02  -- Per military tech above base level
NDefines.NCountry.RAZE_TECH_POWER_DECREASE_MAX = 0.4
NDefines.NCountry.RAZE_PROVINCE_COOLDOWN_YRS = 15
NDefines.NCountry.HRE_MAX_RANK = 2						-- for members
NDefines.NCountry.LAGGINGTECH_CORRUPTION = 0.05
NDefines.NCountry.CORRUPTION_COST = 0.1				-- cost for monthly combat per development
NDefines.NCountry.MAX_CROWN_COLONIES = 9
NDefines.NCountry.PROVINCE_DISCOVERY_PRESTIGE = 0.02
NDefines.NCountry.TARIFF_LIBERTY_INCREASE = 0.1					-- Liberty increase for each % tariffs
NDefines.NCountry.PS_BUY_GENERAL = 100
NDefines.NCountry.PS_BUY_ADMIRAL = 80
NDefines.NCountry.PS_BUY_CONQUISTADOR = 90
NDefines.NCountry.PS_BUY_EXPLORER = 70
NDefines.NCountry.ESTATE_PROVINCE_HAPPINESS_INCREASE = 0
NDefines.NCountry.ESTATE_PROVINCE_HAPPINESS_DECREASE = 1
NDefines.NCountry.ANGRY_THRESHOLD = 35
NDefines.NCountry.HAPPY_THRESHOLD = 60
NDefines.NCountry.INSTITUTION_BONUS_FROM_IMP_DEVELOPMENT = 0
NDefines.NCountry.LIBERTY_DESIRE_DEVELOPED_IN_SUBJECT = 0.001
NDefines.NCountry.PS_ARTILLERY_BARRAGE = 100
NDefines.NCountry.MERCHANT_REPUBLIC_SIZE_LIMIT = 100
NDefines.NCountry.ADVISOR_COST_INCREASE_PER_YEAR = 0.003
NDefines.NCountry.CULTURE_COST_DIFF_ORIGINAL = -40
NDefines.NCountry.CULTURE_COST_DIFF_ADJACENT = -50
--NCountryEnd

--NEconomy

NDefines.NEconomy.OVERSEAS_MIN_AUTONOMY = 60
NDefines.NEconomy.DECREASE_AUTONOMY_STEP = -10
NDefines.NEconomy.INCREASE_AUTONOMY_STEP = 10
NDefines.NEconomy.INCREASE_AUTONOMY_MAX = 90
NDefines.NEconomy.AUTONOMY_CHANGE_DURATION = 7300
NDefines.NEconomy.BASE_YEARLY_INFLATION = 0.01
NDefines.NEconomy.INFLATION_FROM_PEACE_GOLD = 0.03
NDefines.NEconomy.INFLATION_ACTION_REDUCTION = 1
NDefines.NEconomy.MINIMUM_INTERESTS = 1.0
NDefines.NEconomy.BASE_INTERESTS = 6.0
NDefines.NEconomy.LIGHT_SHIP_MAINT_FACTOR = 0.02
NDefines.NEconomy.LIGHT_SHIP_MAINT_FACTOR = 0.02
NDefines.NEconomy.DEBASE_MONTHS_PER_CHARGE = 11
NDefines.NEconomy.ADVISOR_COST = 0.25
--NEconomyEnd

--NMilitary
NDefines.NMilitary.BASE_MP_TO_MANPOWER = 0.1
NDefines.NMilitary.HEAVY_SHIP_COST = 80
NDefines.NMilitary.LIGHT_SHIP_COST = 25
NDefines.NMilitary.GALLEY_COST = 15
NDefines.NMilitary.TRANSPORT_COST = 25
NDefines.NMilitary.INFANTRY_TIME = 90
NDefines.NMilitary.CAVALRY_TIME = 135
NDefines.NMilitary.ARTILLERY_TIME = 180
NDefines.NMilitary.HEAVY_SHIP_TIME = 1460
NDefines.NMilitary.MONTHLY_REINFORCE = 0.1
NDefines.NMilitary.LOOTED_DAYS = 365
NDefines.NMilitary.LOOTED_SCALE = 0.25
NDefines.NMilitary.LOOTED_MAX = 1
NDefines.NMilitary.LOOTED_RECOVERY = 0.05
NDefines.NMilitary.NOMAD_PLAINS_SHOCK_BONUS = 0.15
NDefines.NMilitary.NOMAD_NON_PLAINS_SHOCK_PENALTY = -0.10
NDefines.NMilitary.SUPPLYLIMIT_BASE_MULTIPLIER = 4.0
NDefines.NMilitary.MORALE_RECOVERY_SPEED = 0.1
NDefines.NMilitary.DELIBERATE_RETREAT_MORALE_PENALTY = 0.5
NDefines.NMilitary.OVERRUN_FACTOR_CANNOT_LEAVE = 3
NDefines.NMilitary.LEADER_MAX_PIPS = 7
NDefines.NMilitary.CAV_LOOT = 0.2
NDefines.NMilitary.HEAVY_SHIP_SAILORS_COST = 150
NDefines.NMilitary.LIGHT_SHIP_SAILORS_COST = 50
NDefines.NMilitary.GALLEY_SHIP_SAILORS_COST = 50
NDefines.NMilitary.TRANSPORT_SHIP_SAILORS_COST = 30
NDefines.NMilitary.FORTRESS_COST = 0.35
NDefines.NMilitary.DAYS_PER_SIEGE_PHASE = 30
NDefines.NMilitary.FORT_DEVASTATION_IMPACT = -5
--NMilitaryEnd

--NAI

NDefines.NAI.AI_USES_HISTORICAL_IDEA_GROUPS = 1
NDefines.NAI.REGIMENTS_FOR_CONQUISTADOR = 6
NDefines.NAI.PEACE_TIME_MONTHS = 36
NDefines.NAI.PEACE_INCONCLUSIVE_THRESHOLD = 1
NDefines.NAI.PEACE_TERMS_PROVINCE_OVEREXTENSION_MIN_MULT = 0.5
NDefines.NAI.PEACE_TERMS_PROVINCE_OVEREXTENSION_MIN_MULT = 0.5
NDefines.NAI.DEVELOPMENT_CAP_BASE = 1
NDefines.NAI.DEVELOPMENT_CAP_MULT = 1
NDefines.NAI.PEACE_ALLY_BASE_RELUCTANCE_MULT = 1.5
NDefines.NAI.PEACE_ALLY_EXCESSIVE_DEMANDS_MULT = 1.5
NDefines.NAI.ADVISOR_BUDGET_FRACTION = 0.2
NDefines.NAI.CORRUPTION_BUDGET_FRACTION = 0.4
--NAIEnd

--NGraphics

NDefines.NGraphics.BORDER_WIDTH = 1

--NGraphicsEnd