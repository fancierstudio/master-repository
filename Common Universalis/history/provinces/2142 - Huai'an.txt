# 2142 - Huai'an

owner = MNG
controller = MNG
add_core = MNG
culture = jianghuai
religion = confucianism
capital = "Huai'an"
trade_goods = fish
hre = no
base_tax = 6 

base_production = 6
base_manpower = 3
is_city = yes

extra_cost = 10
discovered_by = chinese
discovered_by = indian
discovered_by = nomad_group
add_permanent_province_modifier = {
	name = huang_he_estuary_modifier
	duration = -1
}

1645.6.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}