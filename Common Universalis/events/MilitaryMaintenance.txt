#land_maintenance = x
#naval_maintenance = x
#x = 0 - 100
#Army Deserted
#Army Recovered
#Navy Deserted
#Navy Recovered

namespace = cumilitary



###Military Upkeep(Reduce)
country_event = {
    id = cumilitary.98
	title = "EVT_HIDDEN"
	desc = "tulip.6.d"
	picture = ECONOMY_eventPicture
	hidden = yes
	trigger = {
	         has_global_flag = test_version
             OR = {
			        AND = {
					       NOT = { land_maintenance = 99 }
						   check_variable = { which = army_upkeep_rate value = 100 } 
					}
					AND = {
					       NOT = { land_maintenance = 90 }
						   check_variable = { which = army_upkeep_rate value = 90 } 
					}
					AND = {
					       NOT = { land_maintenance = 80 }
						   check_variable = { which = army_upkeep_rate value = 80 } 
					}
					AND = {
					       NOT = { land_maintenance = 70 }
						   check_variable = { which = army_upkeep_rate value = 70 } 
					}
					AND = {
					       NOT = { land_maintenance = 60 }
						   check_variable = { which = army_upkeep_rate value = 60 } 
					}
					AND = {
					       NOT = { land_maintenance = 50 }
						   check_variable = { which = army_upkeep_rate value = 50 } 
					}
					AND = {
					       NOT = { land_maintenance = 40 }
						   check_variable = { which = army_upkeep_rate value = 40 } 
					}
					AND = {
					       NOT = { land_maintenance = 30 }
						   check_variable = { which = army_upkeep_rate value = 30 } 
					}
					AND = {
					       NOT = { land_maintenance = 20 }
						   check_variable = { which = army_upkeep_rate value = 20 } 
					}
					AND = {
					       NOT = { land_maintenance = 10 }
						   check_variable = { which = army_upkeep_rate value = 10 } 
					}
			 }	
	}
	mean_time_to_happen = {
	       months = 1
	}
	option = {
	         name = "EVT_HIDDEN"
	         change_variable = { which = army_upkeep_rate value = -10 }
			 #country_event = { id = tulip.7 }
    }	
}

###Military Upkeep(Increase)
country_event = {
    id = cumilitary.97
	title = "EVT_HIDDEN"
	desc = "EVT_HIDDEN"
	picture = ECONOMY_eventPicture
	hidden = yes
	trigger = {
	         has_global_flag = test_version
             OR = {
			        AND = {
					       land_maintenance = 100
						   NOT = { check_variable = { which = army_upkeep_rate value = 100 } } 
					}
			        AND = {
					       land_maintenance = 90
						   NOT = { check_variable = { which = army_upkeep_rate value = 90 } } 
					}
					AND = {
					       land_maintenance = 80
						   NOT = { check_variable = { which = army_upkeep_rate value = 80 } } 
					}
					AND = {
					       land_maintenance = 70
						   NOT = { check_variable = { which = army_upkeep_rate value = 70 } } 
					}
					AND = {
					       land_maintenance = 60
						   NOT = { check_variable = { which = army_upkeep_rate value = 60 } } 
					}
					AND = {
					       land_maintenance = 50
						   NOT = { check_variable = { which = army_upkeep_rate value = 50 } } 
					}
					AND = {
					       land_maintenance = 40
						   NOT = { check_variable = { which = army_upkeep_rate value = 40 } } 
					}
					AND = {
					       land_maintenance = 30
						   NOT = { check_variable = { which = army_upkeep_rate value = 30 } } 
					}
					AND = {
					       land_maintenance = 20
						   NOT = { check_variable = { which = army_upkeep_rate value = 20 } } 
					}
					AND = {
					       land_maintenance = 10
						   NOT = { check_variable = { which = army_upkeep_rate value = 10 } } 
					}
			 }	
	}
	mean_time_to_happen = {
	       months = 3
	}
	option = {
	         name = "EVT_HIDDEN"
	         change_variable = { which = army_upkeep_rate value = -10 }
			 #country_event = { id = tulip.7 }
    }	
}

