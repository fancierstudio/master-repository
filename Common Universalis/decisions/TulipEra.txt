country_decisions = {

### Economic Policy

   choose_economic_policy = {
		potential = {
			OR = {
				government = monarchy
				government = republic
                                technology_group = western
			}
                        OR = {
						        NOT = { has_ruler_flag = chose_economic_policy }
								had_country_flag = {
                                          flag = chose_economic_policy_timer
                                          days = 7300
                                }
						}
                        OR = {
                                NOT = { has_country_flag = chose_economic_policy_timer }
                                had_country_flag = {
                                          flag = chose_economic_policy_timer
                                          days = 3650
                                }
                        }
		}
		allow = {
			stability = -2
                        is_bankrupt = no
                        has_regency = no
		}
		effect = {
                        clr_country_flag = chose_economic_policy_timer 
                        set_ruler_flag = chose_economic_policy
                        set_country_flag = chose_economic_policy_timer 
                        country_event = { id = tulip.0 }
		}
		ai_will_do = {
			factor = 1
		}
   }

   hide_population_growth = {
		potential = {
		        ai = no
                        NOT = {
                                has_country_flag = hide_population_growth
                        }
		}
		allow = {
		}
		effect = {
                        set_country_flag = hide_population_growth
		}
		ai_will_do = {
			factor = 1
		}
   }

   show_population_growth = {
		potential = {
		        ai = no
                        has_country_flag = hide_population_growth
		}
		allow = {
		}
		effect = {
                        clr_country_flag = hide_population_growth
		}
		ai_will_do = {
			factor = 1
		}
   }

}