country_decisions = {

	mughal_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			tag = TIM
			OR = {
				NOT = { exists = MUG }
				ai = no
			}
			OR = {
				culture_group = iranian
				primary_culture = turkmeni
				primary_culture = uzbehk
				primary_culture = kirgiz
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 5
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			NOT = { exists = MUG }
                        total_development = 200
			is_at_war = no
			is_subject = no
                        OR = { 
                                adm_tech = 10
			        government_rank = 3
                                NOT = { government = steppe_horde }
                                NOT = {
                                       num_of_owned_provinces_with = {
			                      value = 15
			                      AND = {
                                                     NOT = { culture_group = hindusthani }
                                                     NOT = { culture_group = eastern_aryan }
                                                     NOT = { culture_group = western_aryan }
                                                     NOT = { culture_group = dravidian }
                                                     NOT = { culture_group = central_indic }
                                              }
                                       }
                                }
                        }
			owns_core_province = 578
			owns_core_province = 507
			owns_core_province = 510
			owns_core_province = 522
			owns_core_province = 524	
		}
		effect = {
			change_tag = MUG
			set_government_rank = 3
			bengal_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = MUG
			}
			hindusthan_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = MUG
			}
			west_india_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = MUG
			}
			deccan_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = MUG
			}
			coromandel_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = MUG
			}
			add_prestige = 25
			
			if = {
				limit = {
					technology_group = nomad_group
				}
				change_technology_group = muslim
			}
			change_government = iqta
			change_unit_type = indian
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

}
